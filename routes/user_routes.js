import express from "express";
import User from "../controllers/user_controller.js";
import bcrypt from "bcryptjs";
import jwt from "jsonwebtoken";
import verifyToken from "../middleware/auth.js";

var user_routes = express.Router();

user_routes.get("/user", verifyToken, async (req, res) => {
  try {
    let data = await User.findById(req.body._id);
    console.log("User: ", data);
    if (!data) {
      res.status(200).send({ success: false, message: "User not found" });
    } else {
      res.status(200).send({
        success: true,
        data,
      });
    }
  } catch (e) {
    res.status(400).send({ e });
    console.log("Error: ", e);
  }
});

user_routes.post("/user", async (req, res) => {
  try {
    const { name, email, password } = req.body;

    if (await User.findOne({ email: email })) {
      res.status(409).send({
        success: false,
        message: "Email is already registered.",
      });
    } else {
      const encryptedPassword = await bcrypt.hash(password, 10);
      const user = await User.create({
        name: name,
        email: email,
        password: encryptedPassword,
      });

      const token = jwt.sign(
        { email_id: email },
        "abcdefghijklmnopqrstuvwxyz",
        {
          expiresIn: "24h",
        }
      );

      delete user['password'];

      res.status(201).send({
        success: true,
        message: "User successfully created",
        user,
        access_token: token,
      });
    }
  } catch (e) {
    res.status(400).send(e.message);
  }
});

user_routes.patch("/user", verifyToken, async (req, res) => {
  try {
    if (
      await User.findOneAndUpdate({ _id: req.body._id }, { $set: req.body })
    ) {
      res.status(200).send({
        success: true,
        message: "User successfully updated",
      });
    } else {
      res.status(200).send({
        success: false,
        message: "User not updated",
      });
    }
  } catch (e) {
    res.status(400).send(e);
    console.log("Error: ", e);
  }
});

user_routes.delete("/user", verifyToken, async (req, res) => {
  try {
    if (await User.findByIdAndDelete(req.body._id)) {
      res.status(200).send({
        success: true,
        message: "User successfully deleted",
      });
    } else {
      res.status(200).send({
        success: false,
        message: "User not found",
      });
    }
  } catch (e) {
    res.status(400).send(e);
    console.log("Error: ", e);
  }
});

user_routes.post("/user/login", async (req, res) => {
  try {
    const { email, password } = req.body;

    const userObj = await User.findOne({
      email,
    });
    if (userObj && (await bcrypt.compare(password, userObj.password))) {
      const token = jwt.sign(
        { email_id: email },
        "abcdefghijklmnopqrstuvwxyz",
        {
          expiresIn: "24h",
        }
      );

      delete userObj['password'];

      res.status(200).send({
        success: true,
        message: "User successfully logged in",
        user: userObj,
        access_token: token,
      });
    } else {
      res.status(200).send({
        success: false,
        message: "Login failed",
      });
    }
  } catch (e) {
    console.log(e.message);
    res.status(400).send(e);
  }
});

export default user_routes;
