import express from "express";
import http from "http";
import user_routes from "./routes/user_routes.js";
import mongoose from "mongoose";
import environment from "dotenv";

environment.config()

const { MONGODB_URL } = process.env;
var mongoDBURL = process.env.MONGODB_URL || MONGODB_URL;

mongoose.connect(mongoDBURL, {
  useNewUrlParser: true,
  useUnifiedTopology: true,
  useCreateIndex: true,
  useFindAndModify: false,
});

const app = express();
app.use(express.json());

app.use("/", user_routes);

const port = process.env.PORT || 3000;
const server = http.createServer(app);

server.listen(port);
